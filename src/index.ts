import { Console } from "console";
 //test
console.log('Hello World!');

//fonction pour additionner 2 nombres pour faire un test 
function add(a: number, b: number) {
    return a + b;
}
let sum = add(2, 3);
console.log(sum);

    
//fonction pour convertir des euros en yens
function conversion(montant: number, deviseEntree: string, deviseSortie: string) {
    let conversionJpy = montant * 130;
    let conversionEuro = montant /130;
    if (deviseEntree === "euro") {
        return conversionJpy + deviseSortie;
    } else if (deviseEntree === "jpy") {
        return conversionEuro + deviseSortie;
    }
}
console.log(conversion(20, "euro", "jpy"));
console.log(conversion(2000, "jpy", "euro"))

function fraisLivraison(poids: number, country: string) {
    if (country === "France") {
    if (poids <= 1) {
        return "les frais de livraisons s'élèvent à 10 euros"
    } else if (poids > 3) {
        return "les frais de livraison s'élèvent à 30 euros"
    } else if (poids <= 3 && poids > 1) {
       return "les frais sont égaux à 20 euros"
    }
   } 
}
console.log(fraisLivraison(2, "France"));


function fraisDouanes(valeurDeclaree: number, paysDeDestination: string): string | undefined {
    if (paysDeDestination === "Canada") {
        if (valeurDeclaree > 20) {
            // Calcul des frais de douane en CAD
            let fraisDouane = valeurDeclaree * 0.15;
            return `Les frais de douane pour le Canada sont de ${fraisDouane} CAD`;
        }
    } else if (paysDeDestination === "Japon") {
        if (valeurDeclaree > 5000) {
            // Calcul des frais de douane en JPY
            let fraisDouane = valeurDeclaree * 0.10;
            return `Les frais de douane pour le Japon sont de ${fraisDouane} JPY`;
        }
    } else if (paysDeDestination === "France") {
        // Aucun frais de douane pour la France
        return "Pas de frais de douane pour la France";
    }

    // Aucun frais de douane pour d'autres pays non spécifiés
    return "Pas de frais de douane pour ce pays";
}

console.log(fraisDouanes(30, "Canada"));
console.log(fraisDouanes(6000, "Japon"));
console.log(fraisDouanes(40, "France"));

  